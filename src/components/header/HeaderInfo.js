import { Grid, Typography } from '@mui/material';

function HeaderInfo() {
    return (
        <Grid item lg={6}>
            <Typography style={{ fontSize: "35px", fontWeight: 900, paddingLeft: "200px" }}>DevCamp</Typography>
        </Grid>
    )
}

export default HeaderInfo;
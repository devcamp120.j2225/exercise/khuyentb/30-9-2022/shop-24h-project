import { Grid, Typography } from '@mui/material';

import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

function LogoComponent() {
    return (
        <Grid item lg={6}>
            <Typography style={{ paddingLeft: "100px", marginTop: "20px" }}>
                <NotificationsNoneIcon sx={{ cursor: "pointer" }}></NotificationsNoneIcon>
                <AccountCircleIcon sx={{ cursor: "pointer" }}></AccountCircleIcon>
                <ShoppingCartOutlinedIcon sx={{ cursor: "pointer" }}></ShoppingCartOutlinedIcon>
            </Typography>
        </Grid>
    )
}

export default LogoComponent;
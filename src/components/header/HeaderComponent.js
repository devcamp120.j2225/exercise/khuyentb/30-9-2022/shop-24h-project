import { Grid } from '@mui/material';
import { Container } from '@mui/system';
import LogoComponent from './HeaderIconNav';
import HeaderInfo from './HeaderInfo';

function HeaderComponent() {
    return (
        <Container className="nav" maxWidth="containerLg">
            <Grid container className='text-center'>
                <HeaderInfo></HeaderInfo>
                <LogoComponent></LogoComponent>
            </Grid>
        </Container>
    )
}

export default HeaderComponent;
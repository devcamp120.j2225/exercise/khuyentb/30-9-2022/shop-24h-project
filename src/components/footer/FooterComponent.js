// import { Grid } from "@mui/material";
import { Grid } from "@mui/material";
import { Container } from "@mui/system";
import FooterProduct from "./FooterProduct";
import FooterService from "./FooterService";
import FooterSocial from "./FooterSocial";
import FooterSupport from "./FooterSupport";

function FooterComponent() {
    return (
        <Container maxWidth="containerLg" style={{paddingBottom: "100px", paddingTop: "70px", backgroundColor: "lightgrey"}}>
            <Grid container spacing ={3}>
                <FooterProduct></FooterProduct>
                <FooterService></FooterService>
                <FooterSupport></FooterSupport>
                <FooterSocial></FooterSocial>
            </Grid>
        </Container>
    )
}

export default FooterComponent;
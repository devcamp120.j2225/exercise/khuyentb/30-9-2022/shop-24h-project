import { Grid, Typography } from "@mui/material";

function FooterService () {
    return (
   
        <Grid item lg={3} style={{paddingLeft: "200px"}}>
        <Typography style={{fontSize: '20px', fontWeight: "bold"}}>SERVICES</Typography>
        <Typography style={{fontSize: "15px"}} className="mt-1">Help Center</Typography>
        <Typography style={{fontSize: "15px"}} className="mt-1">Contact Us</Typography>
        <Typography style={{fontSize: "15px"}} className="mt-1">Product Help</Typography>
        <Typography style={{fontSize: "15px"}} className="mt-1">Warranty</Typography>
        <Typography style={{fontSize: "15px"}} className="mt-1">OrderStatus</Typography>
    </Grid>     
    
    )
}

export default FooterService
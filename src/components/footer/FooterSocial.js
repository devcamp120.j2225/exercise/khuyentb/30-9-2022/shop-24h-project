import { Grid, Typography } from "@mui/material";
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';

function FooterSocial() {
    return (
        <>
            <Grid item lg={3} style={{ paddingLeft: "100px" }}>
                <Typography style={{ fontSize: '30px', fontWeight: "bold" }}>DevCamp</Typography>
                <Grid className="mt-2" >
                    <Typography>
                        <FacebookRoundedIcon style={{paddingRight: "10px"}} fontSize="large"></FacebookRoundedIcon>
                        <InstagramIcon style={{paddingRight: "10px"}} fontSize="large"></InstagramIcon>
                        <YouTubeIcon style={{paddingRight: "10px"}} fontSize="large"></YouTubeIcon>
                        <TwitterIcon style={{paddingRight: "10px"}} fontSize="large"></TwitterIcon>
                    </Typography>
                </Grid>
            </Grid>

        </>


    )
}

export default FooterSocial
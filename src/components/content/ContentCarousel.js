import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import imageCarousel_1 from "../../assets/images/JBL Quantum 100-carousel-1.jpg"
import imageCarousel_2 from "../../assets/images/JBL T510BT-carousel-2.jpg";


function ContentCarousel() {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        
    }
    return (
        <div className="container">
            <Slider {...settings}>
                <div>
                    <div className="container-fluid">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text">
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">JBL Quantum 100</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Ipsum dolor</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn btn-dark mt-4 shop-now">SHOP NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <img className="carousel-image" src={imageCarousel_1} alt="carousel-1" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="container-fluid">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text">
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">JBL T510BT</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Ipsum dolor</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn btn-dark mt-4 shop-now" style={{ borderRadius: "0" }}>SHOP NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <img className="carousel-image" src={imageCarousel_2} alt="carousel-2" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Slider>
        </div>
    )
}

export default ContentCarousel
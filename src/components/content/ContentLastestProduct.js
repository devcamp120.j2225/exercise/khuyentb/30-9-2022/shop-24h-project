import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";

import image_1 from "../../assets/images/JBL Quantum 400-products-1.png";
import image_2 from "../../assets/images/JBL E55BT Key Black-products-2.jpg";
import image_3 from "../../assets/images/JBL JR 310T pink-products-3.jpg";
import image_4 from "../../assets/images/JBL Tune 750BTNC red-products-4.jpg";

import image_5 from "../../assets/images/JBL Horizon-products-5.jpg";
import image_6 from "../../assets/images/JBL Tune 220TWS-products-6.jpg";
import image_7 from "../../assets/images/JBL UA Project Rock-products-7 .jpg";
import image_8 from "../../assets/images/JBL Endurance Sprint-products-8.jpg";

function LatestProductView() {
    return (
        <>
            <Container className="text-center">
                <Grid className="latest-product">
                    <Typography style={{ fontSize: "35px", fontWeight: 900 }}>LATEST PRODUCT</Typography>
                </Grid>
            </Container>
            <Container className="mt-5 text-center">
                <Grid container item spacing={3}>
                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_1}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_2}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_3}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_4}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_5}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_6}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_7}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                    <Grid item xs={12} sm={12} md ={12} lg={3}>
                        <Card sx={{maxWidth: 350}}>
                            <CardActionArea>
                                <CardMedia
                                component="img"
                                image={image_8}
                                height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{marginTop: "30px", fontSize: "20px"}}>JBL Quantum 400</Typography>
                                    <Typography>
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{fontSize: "20px"}}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>

                   
                </Grid>
            </Container>
        </>

    )
}

export default LatestProductView
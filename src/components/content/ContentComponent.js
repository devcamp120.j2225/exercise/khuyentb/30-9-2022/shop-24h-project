import { Container } from "@mui/system";
import ContentCarousel from "./ContentCarousel";
import LatestProductView from "./ContentLastestProduct";
import ViewAll from "./ContentViewAll";

function ContentComponent() {
    return (
        <Container>
            <ContentCarousel></ContentCarousel>
            <LatestProductView></LatestProductView>
            <ViewAll></ViewAll>
        </Container>
    )
}

export default ContentComponent;
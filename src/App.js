import "bootstrap/dist/css/bootstrap.min.css"
import './App.css';
import ContentComponent from "./components/content/ContentComponent";
import FooterComponent from "./components/footer/FooterComponent";
import HeaderComponent from './components/header/HeaderComponent';



function App() {
  return (
    <div>
      <HeaderComponent></HeaderComponent>
      <ContentComponent></ContentComponent>
      <FooterComponent></FooterComponent>
    </div>
  );
}

export default App;
